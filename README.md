# IoT Dashboard (FrontEnd Server)

**IoTDashboard** - A Data Analytics Dashboard based on **[Bootstrap 3](https://github.com/twbs/bootstrap)** framework with data from IoT sensors installed at Latech.

### Prerequisites

1. [nodejs] (https://nodejs.org/en/)
2. [npm - package manager for node] (https://www.npmjs.com/)
3. A web browser (cough don't use Internet Explorer cough cough)
4. [pm2 - container for nodejs webservice] (http://pm2.keymetrics.io/)
5. [bower] (https://bower.io/)

### Installing

- clone the repo
- `bower install` to install all the dependencies
- make sure the backend server is already running on localhost:3000
- Open index.html in web browser to view the dashboard

## Deployment
For deployment in a server, it's better to run the web-service in a separate process manager.

- `npm install pm2 -g` to install pm2
- `pm2 start index.js` to start the server
